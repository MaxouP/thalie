const express = require('express');
const mongoose = require('mongoose');

const items = require('./routes/api/items');
const users = require('./routes/api/users');
const projects = require('./routes/api/projects');

const app = express();

// Bodyparser Middleware
app.use(express.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

// database config
const db = require('./config/keys').mongoURI;

// Connect to mongodb
mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('MongoDb connected'))
    .catch(err => console.log(err));

app.use('/api/items', items);
app.use('/api/users', users);
app.use('/api/projects', projects);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));