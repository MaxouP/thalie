const express = require('express');
const User = require('../../models/User');
const router = express.Router();

// Item model
const Item = require('../../models/User');


/**
 * @route   GET api/user
 * @desc    Get All Users
 * @access  Public
 */
router.get('/', (req, res) => {
    User.find()
        .sort({ date: -1 })
        .then(users => res.json(users));
});

/**
 * @route   POST api/user
 * @desc    Create an user
 * @access  Public
 */
router.post('/', (req, res) => {
    const newUser = new Item({
        name: req.body.name,
        mail: req.body.mail,
        profession: req.body.profession,
        age: req.body.age
    });

    newUser.save().then(user => res.json(user));
});

/**
 * @route   DELETET api/user/:id
 * @desc    Create an user
 * @access  Public
 */
router.delete('/:id', (req, res) => {
    User.findById(req.params.id)
        .then(user => user.remove().then(() => res.json({ success: true })))
        .catch(err => res.status(404).json({ success: false }));
})


module.exports = router;