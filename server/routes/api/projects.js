const express = require('express');
const Project = require('../../models/Project');
const router = express.Router();

// Item model
const Item = require('../../models/Project');


/**
 * @route   GET api/Project
 * @desc    Get All Projects
 * @access  Public
 */
router.get('/', (req, res) => {
    Project.find()
        .sort({ date: -1 })
        .then(Projects => res.json(Projects));
});

/**
 * @route   POST api/Project
 * @desc    Create an Project
 * @access  Public
 */
router.post('/', (req, res) => {
    const newProject = new Item({
        name: req.body.name,
        type: req.body.type,
        creator: req.body.creator,
        participants: req.body.participants,
        description: req.body.description
    });

    newProject.save().then(Project => res.json(Project));
});

/**
 * @route   DELETET api/Project/:id
 * @desc    Create an Project
 * @access  Public
 */
router.delete('/:id', (req, res) => {
    Project.findById(req.params.id)
        .then(Project => Project.remove().then(() => res.json({ success: true })))
        .catch(err => res.status(404).json({ success: false }));
})


module.exports = router;