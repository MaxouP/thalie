const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    mail: {
        type: String,
        required: true,
    },
    age: {
        type: Number,
        required: true,
    },
    profession: {
        type: String,
        required: false,
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
})

module.exports = User = mongoose.model('user', UserSchema)