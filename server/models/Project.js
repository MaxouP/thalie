const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: false,
    },
    creator: {
        type: String,
        required: true,
    },
    participants: {
        type: Array,
        required: false,
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    description:{
        type: String,
        required: false
    }
})

module.exports = Project = mongoose.model('project', ProjectSchema)