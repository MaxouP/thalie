
# Thalie



## Indices

* [Projects](#projects)

  * [http://localhost:5000/api/projects](#1-get-http:localhost:5000apiprojects)
  * [http://localhost:5000/api/projects](#2-post-http:localhost:5000apiprojects)
  * [http://localhost:5000/api/projects/{id}](#3-delete-http:localhost:5000apiprojects{id})

* [Users](#users)

  * [http://localhost:5000/api/iusers](#1-get-http:localhost:5000apiiusers)
  * [http://localhost:5000/api/users](#2-post-http:localhost:5000apiusers)
  * [http://localhost:5000/api/users/{id}](#3-delete-http:localhost:5000apiusers{id})


--------


## Projects



### 1. GET http://localhost:5000/api/projects


Permet d'obtenir tous les projets


***Endpoint:***

```bash
Method: GET
Type: 
URL: http://localhost:5000/api/projects
```



***More example Requests/Responses:***


##### I. Example Request: http://localhost:5000/api/projects



***Status Code:*** 0

<br>



### 2. POST http://localhost:5000/api/projects


Permet de créer un projet.
Il faut lui donner un body avec:
* Le nom
* Le nom du créateur
* Le type de projet
* Les participants
* La description du projet


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: http://localhost:5000/api/projects
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Body:***

```js        
{
    "name": "ProjectName",
    "type": "Theatre",
    "creator": "Paul Martin",
    "participants": ["Jean Dupont", "Robin Schneider", "Paul Martin"],
    "description": "Un projet de theatre comique se déroulant au 19e siecle"
}
```



### 3. DELETE http://localhost:5000/api/projects/{id}


Permet de supprimer un projet via son id.


***Endpoint:***

```bash
Method: DELETE
Type: 
URL: http://localhost:5000/api/projects/{id}
```



## Users



### 1. GET http://localhost:5000/api/iusers


Permet d'obtenir tous les utilisateurs


***Endpoint:***

```bash
Method: GET
Type: 
URL: http://localhost:5000/api/iusers
```



### 2. POST http://localhost:5000/api/users


Permet de créer un utilisateur.
Il faut lui envoyer un body avec:
* Le nom
* L'age
* Sa profession
* Son mail


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: http://localhost:5000/api/users
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Body:***

```js        
{
    "name": "Jean Dupont",
    "mail": "Jeandupont@gmail.com",
    "profession": "cadreur",
    "age": 26
}
```



### 3. DELETE http://localhost:5000/api/users/{id}


Supprime un utilisateur via son id


***Endpoint:***

```bash
Method: DELETE
Type: 
URL: http://localhost:5000/api/users/{id}
```



---
[Back to top](#thalie)