import React, { useEffect, useState } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { v4 as uuid } from 'uuid';
import axios from 'axios';

const UsersList = () => {
    const [users, setUsers] = useState([]);


    useEffect(() => {
        axios
            .get('http://localhost:5000/api/users')
            .then(res => {
                setUsers(res.data);
                console.log(res.data)
            }).catch(err => {
                console.log(err);
            })
    },[])

    return (
        <Container>
            <Button color="dark" style={{ marginBottom: '2rem' }}
                onClick={() => {
                    const name = prompt('Enter new user');
                    if (name) {
                        setUsers([...users, { _id: uuid(), name }]);
                    }
                }}
            >
                Add Item
            </Button>
            <ListGroup>
                <TransitionGroup className="user-list">
                    {users.map(({ _id, name }) => (
                        <CSSTransition key={_id} timeout={500} classNames="fade">
                            <ListGroupItem>
                                <Button
                                    className='remove-btn'
                                    color='danger'
                                    size="sm"
                                    onClick={() => {
                                        setUsers([...users.filter(user => user._id !== _id)])
                                    }}
                                >&times;</Button>
                                {name}
                            </ListGroupItem>
                        </CSSTransition>
                    ))}
                </TransitionGroup>
            </ListGroup>
        </Container>
    );
}

export default (UsersList);