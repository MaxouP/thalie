import { Calendar, momentLocalizer } from 'react-big-calendar';
import 'react-big-calendar/lib/sass/styles.scss';
import moment from 'moment';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import { useState } from 'react';

const localizer = momentLocalizer(moment);
const fakeEventData = [{
    allDay: false,
    start: new Date('December 09, 2020 8:00:00'),
    end: new Date('December 9, 2020 17:00:00'),
    title: 'Tournage clip Paris',
    // Any datas can be added
},
{
    allDay: false,
    start: new Date('December 11, 2020 8:00:00'),
    end: new Date('December 11, 2020 12:00:00'),
    title: 'Séance photo acteur court métrage',
}];

const Planning = () => {
    const [showModal, setShowModal] = useState(false);
    const [showCreateModal, setShowCreateModal] = useState(false);
    const [modalInfo, setModalInfo] = useState(fakeEventData[0]);
    const [fakeEvents, setFakeEvents] = useState(fakeEventData);
    const [timeSlot, setTimeSlot] = useState({ start: new Date(), end: new Date() });
    const [formTitle, setFormTitle] = useState('');

    const selectEvent = (event) => {
        setModalInfo(event);
        setShowModal(true);
    }

    const handleSelect = ({ start, end }) => {
        setTimeSlot({ start, end });
        setShowCreateModal(true);
    }

    const modalEvent = () => {
        return (
            <Modal isOpen={showModal} toggle={() => setShowModal(false)}>
                <ModalHeader>
                    {modalInfo.title}
                </ModalHeader>
                <ModalBody>
                    <h4>Beggining of event:</h4> {modalInfo.start.toDateString()} {modalInfo.start.toLocaleTimeString()}
                    <h4>End of event:</h4> {modalInfo.end.toDateString()} {modalInfo.end.toLocaleTimeString()}
                </ModalBody>
            </Modal>
        )
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const start = timeSlot.start;
        const end = timeSlot.end;
        const event = {
            title: formTitle,
            start,
            end
        }
        setFakeEvents(fakeEvents => [...fakeEvents, event]);
        setShowCreateModal(false);
    }

    const handleChange = (event) => {
        setFormTitle(event.target.value);
    }

    const createModalEvent = () => {
        return (
            <Modal isOpen={showCreateModal} toggle={() => setShowCreateModal(false)}>
                <ModalHeader>
                    Create event
                    </ModalHeader>
                <ModalBody>
                    <h4>Beggining of event:</h4> {timeSlot.start.toDateString()} {timeSlot.start.toLocaleTimeString()}
                    <h4>End of event:</h4> {timeSlot.end.toDateString()} {timeSlot.end.toLocaleTimeString()}
                    <form onSubmit={handleSubmit}>
                        <label>
                            Title:
                            <input type='text' name='title' onChange={handleChange} />
                        </label>
                        <input type="submit" value='Create' />
                    </form>
                </ModalBody>
            </Modal>
        )
    }

    return (
        <div className='container' style={{ marginTop: '5rem', marginBottom: '1rem', height: 850 }}>
            <Calendar selectable defaultView="week" onSelectSlot={(event) => handleSelect(event)} onSelectEvent={event => selectEvent(event)} localizer={localizer} events={fakeEvents} />
            {modalEvent()}
            {createModalEvent()}
        </div>
    )
}

export default Planning;