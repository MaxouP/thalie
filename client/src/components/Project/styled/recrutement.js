import styled from 'styled-components'

const button = styled.div`
  background-color: #FFAADD;
  box-shadow: 3px 3px 10px rgba(0,0,0, 0.1);
  border-radius: 20px;
  width: 70%;
`;

const Wrapper = styled.div`
  background-color: #FFAADD;
  box-shadow: 3px 3px 10px rgba(0,0,0, 0.1);
  border-radius: 20px;
  width: 70%;
`;

export default Wrapper;