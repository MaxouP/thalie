import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import TagInput from './TagInput'
import AddMembre from './List'

const ProjectForm = () => {
  const exampleTags = [{
    id: 1,
    displayValue: 'Bénévole',
  }, {
    id: 2,
    displayValue: 'Danseuse',
  },
  {
    id: 3,
    displayValue: 'Clip',
  }];

  const onTagsChanged = (newTags) => {
      console.log('tags changed to: ', newTags);
  };

  const onInputChanged = (e) => {
      console.log(`input value is now: ${e.target.value}`);
  }
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Step 2
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField
            required
            id="address1"
            name="address1"
            label="Nom du Projet"
            fullWidth
            autoComplete="cc-name"
            value="Clip - Double U"
          />
        </Grid>
        <Grid item xs={12} direction="column">
          <TextField
            required
            id="descriptions"
            name="descriptions"
            label="Descriptions"
            fullWidth
            autoComplete="cc-descriptions"
            value='Clip de musique'
          />
        </Grid>
        </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <TextField required id="cardName" label="Période réalisiation" fullWidth autoComplete="cc-name" value='02/01/2021' />
          <TextField required id="cardName" label="Poste manquant" fullWidth autoComplete="cc-name" value='25/02/2021'/>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="cvv"
            label="Taille du Groupe"
            fullWidth
            autoComplete="cc-csc"
            value='4'
          />
          <AddMembre />
        </Grid>
        <Grid item xs={12}>
          <TagInput tags={exampleTags} onTagsChanged={onTagsChanged} onInputChanged={onInputChanged} />
        </Grid>
        <Grid item xs={12}>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default ProjectForm;