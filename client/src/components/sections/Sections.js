import React, { Fragment } from 'react';

import Home from './Home/Home';
import Contact from './Contact/Contact';
import Card from './Home/Card';

const sections = () => {
  return (
    <Fragment>
      <Home />
      <Card />
      <Contact />
    </Fragment>
  );
};

export default sections;
