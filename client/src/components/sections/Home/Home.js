import React from 'react';

import Section from '../../../HOC/Section';
import bgImage from '../../../assets/img/home.jpeg';
import { Link } from 'react-router-dom';

const home = () => {
  return (
    <Section id='home'>
      <div>
        <div
          className='home-content p-5'
          style={{ backgroundImage: `url(${bgImage})` }}
        >
          <div className='intro container text-center text-light'>
            <h1 className='title'>WELCOME</h1>
            <h2 className='sub-title mb-4'>
            Trouvez des collaborateurs pour mener à bien vos projets.
            </h2>
            <Link to='create' className='btn btn-primary rounded-0 mr-2'>
              Create Project
            </Link>
            <Link to='search' className='btn btn-light text-dark rounded-0'>
              Search Project
            </Link>
          </div>
        </div>
      </div>
    </Section>
  );
};

export default home;
