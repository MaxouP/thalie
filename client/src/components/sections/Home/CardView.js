import React from 'react'
import { Modal, ModalBody, ModalHeader } from 'reactstrap'

const CardView = ({ isOpen, setViewOpen, data }) => {
    return (
        <Modal style={{justifyContent: "end", display: 'flex'}} className="container" centered size="lg" isOpen={isOpen} toggle={() => setViewOpen(false)}>
            <ModalHeader>{data.title}</ModalHeader>
            <ModalBody>
                <img width="300" src={data.image} alt="No Img"/>
                <h5>{data.description}</h5>
            </ModalBody>
        </Modal>
    )
}

export default CardView;