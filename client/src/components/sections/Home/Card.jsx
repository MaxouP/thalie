import React from 'react';

import Section from '../../../HOC/Section';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import MyCard from '../../RealCard';
import theatreImg from '../../../assets/img/theatre.jpg'
import clipImg from '../../../assets/img/clip.jpeg';
import courtmetrageImg from '../../../assets/img/courtmetrage.jpeg'

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(8),
    flexWrap: "nowrap"
  },
}));

const Album = () => {
  const classes = useStyles();
  const cards = [
    {
      image: theatreImg,
      title: 'Le malade imaginaire',
      description: 'Recherche de décorateurs ainsi que maquilleurs pour une version revisitée du malade imaginaire'
    },
    {
      image: clipImg,
      title: 'Clip musique Pop',
      description: 'Recherche des cadreurs et monteurs pour un clip musical de genre pop'
    },
    {
      image: courtmetrageImg,
      title: 'Court métrage',
      description: 'Pour le festival du court métrage amateur de Vichy se déroulant fin Janvier nous sommes à la recherche d\'acteur ainsi que de maquilleurs',
    }
  ];

  return (
    <Section id='card'>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid style={{ flexWrap: 'nowrap' }} container spacing={4}>
          <MyCard data={cards[0]}></MyCard>
          <MyCard data={cards[1]}></MyCard>
          <MyCard data={cards[2]}></MyCard>
        </Grid>
      </Container>
    </Section>
  );
};

export default Album;