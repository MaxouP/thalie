import React, { useState } from 'react';
import CardView from './sections/Home/CardView';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        marginRight: theme.spacing(7),
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
        textOverflow: "ellipsis",
        overflow: "hidden",
        whiteSpace: "noWrap",
        width: '300px'
        
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));
const MyCard = ({ data }) => {
    const classes = useStyles();
    const [viewOpen, setViewOpen] = useState(false);


    return (
        <Card className={classes.card}>
            <CardMedia className={classes.cardMedia} image={data.image} />
            <CardContent className={classes.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                    {data.title}
                </Typography>
                <Typography>
                    {data.description}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" color="primary" onClick={() => setViewOpen(true)}>
                    View
                  </Button>
                  <Button size="small" color="primary">
                    Apply
                  </Button>
                <CardView isOpen={viewOpen} setViewOpen={setViewOpen} data={data} />
            </CardActions>
        </Card>
    )
}

export default MyCard;