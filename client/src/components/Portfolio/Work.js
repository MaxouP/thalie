import React from 'react';
import Project from './Project';
import { portfolio } from './yourdata';

const Work = () => {
    return (
        <div>
            <h1 className='heading'>
            </h1>
            <div className='work-content'>
                <br></br>
                <br></br>
                {portfolio.projects.map((project) => (
                    <Project key={project.id}
                        title={project.title}
                        service={project.service}
                        imageSrc={project.imageSrc}
                        type={project.type}
                        url={project.url}
                    ></Project>
                ))}
            </div>
        </div>
    );
}

export default Work;