import React from 'react';
import Fade from 'react-reveal/Fade';
import ReactPlayer from "react-player";

const Project = ({ title, service, imageSrc, type }) => {
    return (
        <Fade bottom>
            <div style={{ textAlign: 'center' }} className='project'>
                <br></br>
                <br></br>
                <h1>{title}</h1>
                <div>{service}</div>
                <div style={{ justifyContent: "center", display: "flex" }}>
                    {type === 'image' ?
                        <img width="1200px" src={imageSrc} alt={title} /> :
                        <ReactPlayer url={imageSrc} />}
                </div>
            </div>
        </Fade>
    );
}

export default Project;