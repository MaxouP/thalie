import React from 'react';
import Fade from 'react-reveal/Fade';
import { portfolio } from './yourdata'


const Header = () => {
    return (
        <div style={{ textAlign: 'center', marginTop: "5%" }}>
            <h1 className='heading-background'>PORTFOLIO</h1>
            <header>
                <h1>
                    <Fade bottom cascade>{portfolio.name}</Fade></h1>
            </header>
            <Fade bottom>
                <p className='header-title'>
                    {portfolio.headerTagline[0]}<br></br>{portfolio.headerTagline[1]}<br></br>
                    {portfolio.headerTagline[2]}
                    <br></br>
                    <br></br>
                    {/* <button><a href={`mailto:${portfolio.contactEmail}`} rel="noopener noreferrer" >Contact</a></button> */}
                </p>
            </Fade>
        </div>
    );
}

export default Header;