export const portfolio =
    {
        //(Please Do Not Remove The comma(,) after every variable)
        //Change The Website Template
        name :'Paul Schneider',
        headerTagline: [//Line 1 For Header
                        //Line 2 For Header
                        'Cadreur/Photographe',
                        //Line 3 For Header
                        'Paris, France',
    ],
        //Contact Email
        contactEmail:'paul.shneider@thalie.com',
        // Add Your About Text Here
        abouttext: "Jeune homme de 28 ans j'exerce le métier de cadreur depuis 5 ans. Depuis mes 8 ans, j'aime prendre des photos de mon environnement ainsi que de filmer des situations quotidiennes.",
        aboutImage:'https://images.unsplash.com/photo-1521587765099-8835e7201186?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
       //Change This To Hide The Image of About Section (True Or False)
       ShowAboutImage:true,//true or false (Change Here)
       // Change Projects Here 
       projects:[
           {
                id: 1,//DO NOT CHANGE THIS
                title:'Pavillon d\'or de Kyoto', //Project Title - Add Your Project Title Here
                service:'Photo prise avec un Reflex Canon EOS 700D avec un objectif 18-135 IS STM', // Add Your Service Type Here
                //Project Image - Add Your Project Image Here
                imageSrc:'https://www.kanpai.fr/sites/default/files/styles/big_header_lg/public/uploads/2020/06/kinkaku-ji-4.jpg',
                type: 'image',
                //Project URL - Add Your Project Url Here
            },
            {
                id: 2,//DO NOT CHANGE THIS (Please)😅
                title: 'Orage à Paris',
                service: 'Photo prise avec un Reflex Canon EOS 700D avec un objectif 18-135 IS STM',
                imageSrc: "https://medias.liberation.fr/photo/528395-une-personne-sous-un-orage-le-17-juin-2013-a-paris.jpg?modified_at=1374643374&width=960",
                type: 'image',
            },
                    /* If You Want To Add More Project just Copy and Paste This At The End (Update the id Respectively) */
            {
                id: 3,
                title: 'Court métrage Chien bleu',
                service: 'J\'ai participé au tournage en tant que cadreur/cameran',
                imageSrc: "https://www.youtube.com/watch?v=bAttTIb5v_A",
                type: 'video',
            }
        ],
       /* social: [
            // Add Or Remove The Link Accordingly
            {   name:'Github',
                url:'https://github.com/chetanverma16'},
            {
                name: 'Behance',
                url: 'https://www.behance.net/chetanverma'
            },
            {
                name: 'Dribbble',
                url: 'https://dribbble.com/chetanverma'
            },
            {
                name: 'Instagram',
                url: 'https://www.instagram.com/cv.uidesign/'
            }

        ]*/
    };