import React from "react";
import Project from './Project';
import Header from './Header';
import Work from './Work';
import About from './About'

const Portfolio = () => {
    return(
            <div>
                <Header />
                <About />
                <Project />
                <Work />
            </div>
        );
    }

export default Portfolio;