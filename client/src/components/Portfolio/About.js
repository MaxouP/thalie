import React from 'react';
import Fade from 'react-reveal/Fade';
import { portfolio } from './yourdata';

const About = () => {
    return (
        <div style={{ textAlign: 'center' }} className='about'>
            <div className='about-content'>
                <h1><Fade bottom cascade>About.</Fade></h1>
                <Fade bottom>
                    <p>{portfolio.abouttext}</p>
                </Fade>
            </div>
            {portfolio.ShowAboutImage ? <img src={portfolio.aboutImage} alt='about iamge'></img> : null}
        </div>
    );
}

export default About;