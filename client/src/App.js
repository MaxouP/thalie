import 'bootstrap/dist/css/bootstrap.min.css'
import './scss/main.scss'
import './App.css'
import React from 'react';

import Layout from './components/Layout/Layout';
import CreateProjet from './components/Project/CreateProject';
import SearchProjet from './components/Project/SearchProject';
import Portfolio from './components/Portfolio/Portfolio';
import Footer from './components/UI/Footer/Footer';
import Header from './components/UI/Header/Header';
import Planning from './components/Calendar/Calendar';
import { BrowserRouter as Router, Route } from 'react-router-dom';


const App = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <Route exact path='/' component={props => <Layout {...props} />} />
        <Route path='/create' component={props => <CreateProjet {...props} />} />
        <Route path='/search' component={props => <SearchProjet {...props} />} />
        <Route path='/portfolio' component={props => <Portfolio {...props} />} />
        <Route path='/planning' component={props => <Planning {...props} />} />
        <Footer />
      </Router>

    </div>
  );
}

export default App;
